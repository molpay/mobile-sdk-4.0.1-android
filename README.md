MOLPay Mobile SDK 4.0.1 for Android
=================================

Latest updates on 2014/05/05

- Multicurrency support

Updates on 2014/04/21

- New authentication method implemented
- Native app UI for credit card payment
- Secure 1-click payment (tokenization) supported


Prerequisites

1. MOLPay merchant ID
2. Merchant App Name
3. Verify Key
4. API username
5. API password

Note: Please get these information from sales or support team
